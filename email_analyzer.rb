
def buzzword_check(arg)
  buzzwords = %w{I I've I'd me I'll I'm}
  buzzwords.include? arg
end


text = "
    Dear Dr. Pennebaker:

    I was part of your Introductory Psychology class last semester. I have enjoyed your lectures and I've learned so much. I received an email from you about doing some research with you. Would there be a time for me to come by and talk about this?
    I've heard so much. I'm excited.
    Pam"

words = text.split(/[^a-zA-Z']/).delete_if { |word| word == '' }
word_count = words.count

def buzzword_count(arg)
  arg.select { |word| buzzword_check(word) == true }.count
end

def find_percentage(number_of_buzzwords, word_count)
  number_of_buzzwords / word_count.to_f * 100
end


number_of_buzzwords = buzzword_count(text)
find_percentage(number_of_buzzwords, word_count)


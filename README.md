## Status Message

Are you playing low or high status?

Determine the frequency of first-person pronouns in your email. See the application [hosted on Heroku](http://stark-mountain-8082.herokuapp.com/).

This project is inspired by the research of James Pennebaker. I first came across his work
in an NPR segment, ["The secret life of pronouns"](http://www.npr.org/blogs/health/2014/09/01/344043763/our-use-of-little-words-can-uh-reveal-hidden-interests).

***
### To run this program:
* `git clone` https://kari_mah@bitbucket.org/kari_mah/status-message.git
* `bundle install` to install necessary gems
* `ruby app.rb` to run the app on your local machine at http://localhost:4567

***
### TODO
* fix .powrc so I can see app at http://status-message.dev
* in the processed email, highlight the first-person pronouns
* use Gmail API to access user's account (use OAuth2)
* retrieve and analyze messages sent in the last 30 days
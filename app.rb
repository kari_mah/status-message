require 'rubygems'
require 'sinatra'

get '/' do
  erb :index
end

post '/' do


  def buzzword_check(arg)
    buzzwords = %w{I I've I'd me I'll I'm my mine i i've i'd me i'll i'm}
    buzzwords.include? arg
  end


  @text = params[:email]

  words = @text.split(/[^a-zA-Z']/).delete_if { |word| word == '' }
  word_count = words.count

  def buzzword_count(arg)
    arg.select { |word| buzzword_check(word) == true }.count
  end

  def find_percentage(number_of_buzzwords, word_count)
    ratio = number_of_buzzwords / word_count.to_f
    ratio.round(4) * 100
  end


  number_of_buzzwords = buzzword_count(words)
  @percentage = find_percentage(number_of_buzzwords, word_count)

  erb :index
end
